# Cypress Test for Ecooo Web
Cypress test to automate the testing of the Ecooo Web

# Configuration
In order to run the test correctly the web url must be set in cypress.json

# Set up project

Run the command ```npm update``` to download the dependencies. Then run ```npm run cypress:open``` to run the cypress GUI and launch the tests.