import { faker } from '@faker-js/faker'

faker.setLocale('es');

const REQUIRED_FIELDS = ['firstName', 'lastName', 'email', 'phone', 'address', 'city', 'zip', 'gdpr', 'none']

function generateContact(skip) {
    let contact = {}
    contact.firstName = faker.name.firstName(),
    contact.lastName = `${faker.name.lastName()} ${faker.name.lastName()}`,
    contact.email = faker.internet.email(contact.firstName, contact.lastName),
    contact.phone = faker.phone.phoneNumber('#########'),
    contact.address = faker.address.streetAddress(),
    contact.addressExtra = faker.address.secondaryAddress(),
    contact.city = faker.address.city(),
    contact.state = 'Madrid'
    contact.zip = faker.address.zipCode('#####')

    contact[skip] = '';
    return contact;
}

describe('Formulario de solicitud de presupuesto de autoconsumo individual', () => {
    beforeEach(() => {
        cy.visit('/presupuesto-autoconsumo-hogares/');
    });

    REQUIRED_FIELDS.forEach(skip => {
        const description = skip !== 'none' ? `El campo ${skip} es obligatorio` : 'Envio correcto';
        const tags = skip !== 'none' ? ['required-validation'] : ['success'];
        it(description, { tags}, () => {

            const contact = generateContact(skip);
            cy.getByLabel('Nombre').focus();
            
            cy.focused().typeSafe(contact.firstName).should('have.value', contact.firstName)
            .tab().typeSafe(contact.lastName).should('have.value', contact.lastName)
            .tab().typeSafe(contact.email).should('have.value', contact.email)
            .tab().typeSafe(contact.phone).should('have.value', contact.phone)
            .tab().typeSafe(contact.address).should('have.value', contact.address)
            .tab().typeSafe(contact.addressExtra).should('have.value', contact.addressExtra)
            .tab().typeSafe(contact.city).should('have.value', contact.city)
            .tab().selectSafe(contact.state).should('have.value', contact.state)
            .tab().typeSafe(contact.zip).should('have.value', contact.zip)
            .tab()
            .tab().typeSafe(faker.lorem.sentence())
            
            if (skip !== 'gdpr') 
                cy.getByLabel('He leído y acepto la política de protección de datos*.').click();
            cy.getByLabel('Acepto recibir información comercial sobre las ofertas y promociones de Ecooo, en base a nuestra la política de protección de datos.').click();

            //Send form
            cy.contains('ENVIAR').click()
            if (skip !== 'none'){
                cy.contains('Hubo un problema con tu envío. Por favor, revisa los siguientes campos.')
            } else {
                cy.contains('Hemos recibido correctamente tu solicitud de contacto.')
            }
        });
    })
});