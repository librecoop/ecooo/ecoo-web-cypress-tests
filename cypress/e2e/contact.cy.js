import { faker } from '@faker-js/faker'

faker.setLocale('es');

const REQUIRED_FIELDS = ['firstName', 'lastName', 'email', 'related', 'question', 'gdpr', 'none'];

function generateContact(skip){
  let contact = {};
  contact.firstName = `${faker.name.firstName()} ${faker.name.middleName()}`;
  contact.lastName = `${faker.name.lastName()} ${faker.name.lastName()}`;
  contact.email = faker.internet.email(contact.firstName, contact.lastName);
  contact.phone = faker.phone.phoneNumber()
  contact.subject = faker.lorem.slug()
  contact.question = faker.lorem.paragraph()

  contact[skip] = '';
  return contact;
}


describe('Formulario de Contacto', () => {
  REQUIRED_FIELDS.forEach((skip) => {
    const testDescription = skip === 'none' ? 'Prueba de envio correcto del formulario' : `Prueba que el campo ${skip} es requerido`
    const tags = skip === 'none' ? ['success'] : ['required-validation'];
    it(testDescription, { tags } ,() => {
      cy.visit('/contacto/');
      const contact = generateContact(skip);
      //Set name
      cy.get('input[placeholder="Nombre"]')
        .should('have.attr', 'aria-required', 'true')
        .typeSafe(contact.firstName)
        .should('have.value', contact.firstName);

      //Set surname
      cy.get('input[placeholder="Apellidos"]')
        .should('have.attr', 'aria-required', 'true')
        .typeSafe(contact.lastName)
        .should('have.value', contact.lastName);

      //Set email
      cy.getByLabel('E-mail')
      .should('have.attr', 'aria-required', 'true')  
      .typeSafe(contact.email)
      .should('have.value', contact.email);

      //Set phone number
      cy.get('input[placeholder="Teléfono"]')
        .typeSafe(contact.phone)
        .should('have.value', contact.phone);
  
      if (skip !== 'related')
        //Set related
        cy.getByLabel('Consulta relacionada con')
        .should('have.attr', 'aria-required', 'true')
        .select("Inversión en renovables");

      //Set subject
      cy.get('input[placeholder="Asunto"]')
        .typeSafe(contact.subject)
        .should('have.value', contact.subject);
      
      //Set question
      cy.getByLabel('Escribe aquí tu consulta')
        .should('have.attr', 'aria-required', 'true')
        .typeSafe(contact.question)
        .should('have.value', contact.question);

      if (skip !== 'gdpr') {
        //Set GDPR
        cy.getByLabel('He leído y acepto la política de protección de datos*.')
        .click()
      }

      //Set comunications
      cy.getByLabel('Acepto recibir información comercial sobre las ofertas y promociones de Ecooo, en base a nuestra la política de protección de datos.').click()
  
      //Send form
      cy.contains('ENVIAR').click()
      if (skip !== 'none'){
        cy.contains('Hubo un problema con tu envío. Por favor, revisa los siguientes campos.')
      } else {
        cy.contains('Hemos recibido correctamente tu solicitud de contacto.')
      }
    })
  
  })
})
