import { faker } from '@faker-js/faker'

faker.setLocale('es');

const CLIENT_TYPE = {
  INDIVIDUAL: 'Persona individual',
  MINOR: 'Menor de edad',
  MARRIAGE: 'Matrimonio',
  COMPANY: 'Empresa'
}

const PAYMENT_TYPE = {
  CARD: 'Pago con tarjeta',
  TRANSFER: 'Pago mediante transferencia'
}

const INVESTMENT_TYPE = {
  SHORT: 'Corto plazo',
  LONG: 'Largo plazo'
}

const GENDER_OPTIONS = ['Masculino', 'Femenino', 'Otro']

const REQUIRED_FIELDS = [ 'firstName', 'lastName', 'dni', 'address', 'city', 'state', 'zip', 'country', 'amount', 'gdpr'];

function generateContact(clientType, skip) {
  let contact = { }
  contact.firstName = clientType === CLIENT_TYPE.COMPANY ? faker.company.companyName() : faker.name.firstName();
  contact.lastName= `${faker.name.lastName()} ${faker.name.lastName()}`;
  contact.dni= `${faker.datatype.number(99999999)}${faker.random.alpha({ upcase: true })}`;
  contact.birthday= faker.date.past(100);
  if (clientType === CLIENT_TYPE.INDIVIDUAL || clientType === undefined) {
    contact.email = faker.internet.email(contact.firstName, contact.lastName);
    contact.phone = faker.phone.phoneNumber('#########');
    contact.address = faker.address.streetAddress();
    contact.addressExtra = faker.address.secondaryAddress();
    contact.city= faker.address.city();
    contact.state = faker.address.state();
    contact.zip = faker.address.zipCode('#####');
    contact.country = faker.address.country();
    contact.amount = faker.datatype.number({min: 1000});
    contact.aboutUs = 'Otro';
  }
  if (skip)
    contact[skip] = '';
  return contact;
}

function fillInvestmentForm(clientType, skip, contact, secondContact) {

  cy.getByLabel('Soy*').select(clientType).wait(500).tab();

  if(clientType === CLIENT_TYPE.COMPANY){
    //Set company data
    cy.focused().should('have.attr', 'aria-required', 'true').typeSafe(secondContact.firstName).should('have.value', secondContact.firstName)
    .tab().should('have.attr', 'aria-required', 'true').typeSafe(secondContact.dni).should('have.value', secondContact.dni).tab();
  }
  //Set first contact
  // Set name
  cy.focused().should('have.attr', 'aria-required', 'true').typeSafe(contact.firstName).should('have.value', contact.firstName)
  // Set last name
  .tab().should('have.attr', 'aria-required', 'true').typeSafe(contact.lastName).should('have.value', contact.lastName)
  // Set DNI
  .tab().should('have.attr', 'aria-required', 'true').typeSafe(contact.dni).should('have.value', contact.dni)
  // Set gender
  .tab().select(GENDER_OPTIONS[Math.floor(Math.random() * GENDER_OPTIONS.length)])
  //Set Date of Birth
  .tab().typeSafe(contact.birthday.getDate()).should('have.value', contact.birthday.getDate())
  .tab().typeSafe(contact.birthday.getMonth()+1).should('have.value', contact.birthday.getMonth()+1)
  .tab().typeSafe(contact.birthday.getFullYear()).should('have.value', contact.birthday.getFullYear());

  if (clientType === CLIENT_TYPE.MARRIAGE || clientType === CLIENT_TYPE.MINOR) {
    //Set second contact
    // Set name
    cy.focused().tab().should('have.attr', 'aria-required', 'true').typeSafe(secondContact.firstName).should('have.value', secondContact.firstName)
    // Set last name
    .tab().should('have.attr', 'aria-required', 'true').typeSafe(secondContact.lastName).should('have.value', secondContact.lastName)
    // Set DNI
    .tab().should('have.attr', 'aria-required', 'true').typeSafe(secondContact.dni).should('have.value', secondContact.dni)
    // Set gender
    .tab().select(GENDER_OPTIONS[Math.floor(Math.random() * GENDER_OPTIONS.length)])
    //Set Date of Birth
    .tab().typeSafe(secondContact.birthday.getDate()).should('have.value', secondContact.birthday.getDate())
    .tab().typeSafe(secondContact.birthday.getMonth()+1).should('have.value', secondContact.birthday.getMonth()+1)
    .tab().typeSafe(secondContact.birthday.getFullYear()).should('have.value', secondContact.birthday.getFullYear());
  }


  //Set email
  cy.focused().tab().should('have.attr', 'aria-required', 'true').typeSafe(contact.email).should('have.value', contact.email)
  //Set phone number
  .tab().should('have.attr', 'aria-required', 'true').typeSafe(contact.phone).should('have.value', contact.phone)
  //Set Adress
  .tab().should('have.attr', 'aria-required', 'true').typeSafe(contact.address).should('have.value', contact.address)
  .tab().typeSafe(contact.addressExtra).should('have.value', contact.addressExtra)
  //Set city
  .tab().should('have.attr', 'aria-required', 'true').typeSafe(contact.city).should('have.value', contact.city)
  //Set state
  .tab().should('have.attr', 'aria-required', 'true').typeSafe(contact.state).should('have.value', contact.state)
  //Set zicode
  .tab().should('have.attr', 'aria-required', 'true').typeSafe(contact.zip).should('have.value', contact.zip)
  //Set country
  .tab().should('have.attr', 'aria-required', 'true').typeSafe(contact.country).should('have.value', contact.country)

  //Set investment amount
  .tab().tab().should('have.attr', 'aria-required', 'true').typeSafe(contact.amount).should('have.value', contact.amount)

  // Set additional info
  .tab().select(contact.aboutUs)
  .tab().typeSafe(faker.lorem.sentence())

  //Set GDPR
  if (skip !== 'gdpr')
    cy.getByLabel('He leído y acepto la política de protección de datos*.').click();
  //Set comunications
  cy.getByLabel('Acepto recibir información comercial sobre las ofertas y promociones de Ecooo, en base a nuestra la política de protección de datos.').click();

}

function completeFunnelCard() {
    // Realizar Pago
    cy.url().should('include', '/formulario-inversiones-realizar-pago');
    cy.contains('Pagar con tarjeta').click();
    cy.contains('PAGAR CON TARJETA').click();
    cy.url().should('include', 'https://sis-t.redsys.es:25443/sis/realizarPago');
    cy.get('#Sis_Numero_Tarjeta').typeSafe(4548816134581156).should('have.value', 4548816134581156);
    cy.get('#Sis_Caducidad_Tarjeta_Mes').select(12);
    cy.get('#Sis_Caducidad_Tarjeta_Anno').select(34);
    cy.get('#Sis_Tarjeta_CVV2').typeSafe('123').should('have.value', '123');
    cy.contains('Aceptar').click();
    cy.contains('Enviar').click();
    cy.contains('Continuar').click();
    cy.url().should('include', '/formulario-inversiones-firmar-contrato-tarjeta');
}

function completeFunnelTransfer(){
  // Realizar Pago
  cy.url().should('include', '/formulario-inversiones-realizar-pago');
  cy.contains('Pagar mediante transferencia bancaria').click();
  cy.contains('FINALIZAR').click();
  cy.url().should('include', '/formulario-inversiones-firmar-contrato-transferencia');
}

describe('Formulario de Inversion', () => {
  beforeEach(() => {
    cy.visit('/formulario-inversiones/');
  })

  Object.values(CLIENT_TYPE).forEach(clientType => {
    describe(clientType, () => {
      Object.values(INVESTMENT_TYPE).forEach(investmentType => {
        describe(investmentType, () => {
          let requiredFields = REQUIRED_FIELDS;
          if (clientType === CLIENT_TYPE.MARRIAGE || clientType === CLIENT_TYPE.MINOR)
            requiredFields = ['secondfirstName', 'secondlastName', 'seconddni', ...REQUIRED_FIELDS];
            else if (clientType === CLIENT_TYPE.COMPANY)
            requiredFields = ['secondfirstName', 'seconddni', ...REQUIRED_FIELDS];
          requiredFields.forEach(skip => {
            it(`El campo ${skip} es obligatorio`, {tags: ['required-validation', skip, clientType, investmentType]}, () => {
              cy.contains(`Inversión a ${investmentType.toLowerCase()}`).click();
              cy.url().should('include', investmentType===INVESTMENT_TYPE.SHORT ? '/formulario-inversiones-corto' : '/formulario-inversiones-largo');

              const contact = generateContact(CLIENT_TYPE.INDIVIDUAL, skip.includes('second') ? 'none' : skip);
              let secondContact;
              if (clientType !== CLIENT_TYPE.INDIVIDUAL) {
                secondContact = generateContact(clientType, skip.includes('second') ? skip.replace('second','') : 'none');
              }

              fillInvestmentForm(clientType, skip, contact, secondContact);

              //Send form
              cy.contains('ENVIAR').click()
              cy.contains('Hubo un problema con tu envío. Por favor, revisa los siguientes campos.')
            });
          })
          Object.values(PAYMENT_TYPE).forEach(paymentType => {
            it(paymentType, { tags: ['success', clientType, investmentType]}, () => {
              cy.contains(`Inversión a ${investmentType.toLowerCase()}`).click();
              cy.url().should('include', investmentType===INVESTMENT_TYPE.SHORT ? '/formulario-inversiones-corto' : '/formulario-inversiones-largo');

              const contact = generateContact();
              let secondContact;
              if (clientType !== CLIENT_TYPE.INDIVIDUAL) {
                secondContact = generateContact(clientType);
              }

              fillInvestmentForm(clientType, 'none', contact, secondContact);

              //Send form
              cy.contains('ENVIAR').click()

              if (paymentType === PAYMENT_TYPE.CARD) {
                completeFunnelCard();
              } else {
                completeFunnelTransfer();
              }

            });
          });
        });
      });
    });
  });
});
