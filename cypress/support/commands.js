Cypress.Commands.add('getByLabel', (label) => {
  cy.contains('label', label)
    .invoke('attr', 'for')
    .then((id) => {
      cy.get('#' + id)
    })
})

Cypress.Commands.add('typeSafe',{ prevSubject: true }, (subject, text) => {
  if (text !== undefined && text !== '')
    return cy.wrap(subject).type(text);
  return subject;
});

Cypress.Commands.add('selectSafe',{ prevSubject: true}, (subject, text) => {
  if (text !== undefined && text !== '')
    return cy.wrap(subject).select(text);
  return subject;
});